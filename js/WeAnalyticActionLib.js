var WeAnalyticEnhancedECommerce = {
    ProductFieldObject: ['id', 'name', 'category', 'brand', 'variant', 'price', 'quantity', 'coupon', 'list', 'position', 'dimension1'],
    OrderFieldObject: ['id', 'affiliation', 'revenue', 'tax', 'shipping', 'coupon', 'list', 'step', 'option'],
    flatten: function(target){
      var delimiter = '.';
      var output = {};

      function step(object, prev) {
        for (var key in object) {
          if (object.hasOwnProperty(key)) {
            var value = object[key];
            var type = Object.prototype.toString.call(value);

            if (value == null) continue;

            var newKey = prev
              ? prev + delimiter + key
              : key;

            // leave booleans, numbers, strings and dates as is
            if (type === '[object Boolean]' || type === '[object Number]' || type === '[object String]' || type === '[object Date]') {
              output[newKey] = value;
            } else if (type !== '[object Object]' && type !== '[object Array]') {
            // convert non objects/arrays to strings
              output[newKey] = value.toString();
            } else {
              step(value, newKey);
            }
          }
        }
      }

      step(target);

      return output;
    },

    buildProduct: function(Product){
        var Products = {};
        if(Product != null) {
            for (var productKey in Product) {
                for (var i = 0; i < this.ProductFieldObject.length; i++) {
                    if (productKey.toLowerCase() == this.ProductFieldObject[i]) {
                        if (Product[productKey] != null) {
                            Products[productKey.toLowerCase()] = Product[productKey];
                        }
                    }
                }
            }
        }

        return Products;
    },

    buildOrder: function(Order){
        var Orders = {};
        if (Order != null) {
			for (var orderKey in Order) {
				for (var j = 0; j < this.OrderFieldObject.length; j++) {
					if (orderKey.toLowerCase() == this.OrderFieldObject[j]) {
						Orders[orderKey.toLowerCase()] = Order[orderKey];
					}
				}
			}
		}

        return Orders;
    },

    loginUser: function(cuid){
        webengage.user.login(cuid);
    },

    setFullName: function(firstName, lastName){
        webengage.user.setAttribute({"we_first_name": firstName, "we_last_name": lastName});
    },

    setUserAttribute: function(attrName, attrValue){
        webengage.user.setAttribute(attrName, attrValue);
    },

	addProductDetailView: function(Product) {
		webengage.track('product-detail-view', Product);
	},

	addToCart: function(Product) {
		webengage.track('product-add-from-cart', Product);
	},

	removeFromCart: function(Product) {
		webengage.track('product-remove-from-cart', Product);
	},

    addProductClick: function(Product) {
		var ClickPoint = jQuery('a[href$="' + Product.url + '"].quick-view');

        if(ClickPoint.length > 0) {
            ClickPoint.on("click", function() {
                this.add(Product);
                webengage.track('product-quick-view', this.flatten(Product.list));
            });
        }
	},

    addProductViewByHttpReferal: function(Product) {
        var Products = this.buildProduct(Product);
        webengage.track('product-view-by-http-referral', this.flatten(Products));
	},

    add: function(Product, Order, Impression) {
        var Products = {};
		var Orders = {};

		if (Product != null) {
			if (Impression && Product.quantity !== undefined) {
				delete Product.quantity;
			}

            this.Products = this.buildProduct(Product);
		}

		if (Order != null) {
            this.Orders = this.buildOrder(Order);
		}

		if (Impression) {
			webengage.track('product-viewed', this.flatten(Products));
		} else {
			webengage.track('product-added-to-cart', this.flatten(Products));
		}
	},

    addCheckout: function(Step) {
		webengage.track('checkout', {'step': Step});
	},

    addProductView: function(Product) {
        webengage.track('product-view');
	},
};
