<?php
/**
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA &gt;contact@prestashop.com&lt;
*  @copyright 2007-2014 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
?>
<div class="wrap Configuration">
    {if isset($message) && $message|strlen gt 0}
    <div id="message" class="updated mag_message_box" style="font-size:16px;">
      <p class="mag_message_box_text">{$message|escape:'htmlall':'UTF-8'}</p>
    </div>
  {elseif isset($error_message) && $error_message|strlen gt 0}
    <div id="message" class="weerror mag_message_box">
      <p class="mag_message_box_text">{$error_message|escape:'htmlall':'UTF-8'}</p>
    </div>
  {elseif $m_widget_status ne '' && $m_widget_status eq 'PENDING'}
    {assign var="discard_url" value="$main_url&weAction=discardMessage&webengage_license_code=$m_license_code_old"}
    <div id="message" class="weerror mag_message_box">
      <p class="mag_message_box_text">Your WebEngage plugin is not active currently because the email associated with your account needs to be verified.
        We have sent you the activation email already, please follow the instructions therein. In case you haven't received the email, <a class="resend-email-link" href="#">click here to resend the activation email</a>.<span class="discard-msg">[ <a onclick="return confirm('Do you really wish to discard this message?');" href="{$discard_url|escape:'htmlall':'UTF-8'}" title="Discard this message">discard</a> ]</span></p>
    </div>
  {/if}
  <div class="webengage-container">
    <div class="webengage-logo-container">
      <span/>
    </div>

    {if !$m_license_code_old || $m_license_code_old eq ''}
      <div class="webengage-form-container">
        <div class="webengage-form inline-forms">
          <h3>New to WebEngage? Create your account</h3>
          <hr/>
            {assign var="tempMainUrl" value="$main_url&weAction=activate"}
            {assign var="src_url" value="$secure_webengage_host/thirdparty/signup.html?next={$next_url|escape:'url'}&nm=$user_full_name&em=$email&domain=$domain_name&activationUrl={$tempMainUrl|escape:'url'}&channel=prestashop"}
          <div id="webengage-loading-info">Loading, please wait ...</div>
          <iframe src="{$src_url|escape:'htmlall':'UTF-8'}" class="webengage-iframe signup-iframe" id="we-signup-iframe" marginheight="0" frameborder="0" style="height:0;background-color:transparent;" allowTransparency="true"></iframe>
        </div>

        <div class="webengage-form inline-forms">
          <h3>Already a Webengage user? Add license code for {$domain_name|escape:'htmlall':'UTF-8'}</h3>
          <hr>
          <p>Copy the license code for {$domain_name|escape:'htmlall':'UTF-8'} from your <a target="_blank" class="external_icon" href="{$webengage_host}/publisher/dashboard">WebEngage Dashboard</a> and paste it here.</p>
            <form method="post" action="{$main_url|escape:'htmlall':'UTF-8'}">
        <label for="webengage_license_code"><b>Your WebEngage License Code</b></label>
        <input id="webengage_license_code" type="text" name="webengage_license_code" placeholder="License Code" value="{$m_license_code_old|escape:'htmlall':'UTF-8'}"/>
        <input type="hidden" value="wp-save" name="weAction" />
        <input type="hidden" value="submit" name="submit"/>
        <button type="submit">Save</button>
      </form>
        </div>

        <br class="clear"/>
      </div>
    {else}
      <div class="webengage-form-container">
        <form method="post" action="{$main_url|escape:'htmlall':'UTF-8'}">
          <label for="webengage_license_code"><b>Your WebEngage License Code</b></label>
          <input readonly="true" id="webengage_license_code" type="text" name="webengage_license_code" placeholder="License Code" value="{$m_license_code_old|escape:'htmlall':'UTF-8'}"/>
          <input type="hidden" value="wp-save" name="weAction"/>
          <input type="hidden" value="submit" name="submit"/>
          <span><a id="update-license-code-link" href="#" style="color:#21759B;">Change</a></span>
              <span>
                Or,  <a class="form-submit" href="{$main_url|escape:'htmlall':'UTF-8'}&weAction=reset" style="color:#21759B;" onclick="return confirm('Do you wish to change your email or domain name for this plugin?');">Reset the license code</a>
              </span>
          </form>
          <script type="text/javascript">
            var changeLink = document.getElementById("update-license-code-link");
            changeLink.onclick = function(){
              document.getElementById("webengage_license_code").removeAttribute("readonly");
              this.parentNode.innerHTML = '<button type="submit">Save</button>';
              return false;
            }
          </script>
      </div>
      
      <div class="webengage-instructions">
        Your WebEngage widget is installed. What's next?
        <ol>
          <li>
            Manage this widget from your <a href="http://webengage.com/publisher/dashboard">WebEngage dashboard</a>. List of things you can do -
            <ul style="padding-left:30px;">
              <li style="margin-top:6px;"><b>Feedback</b>: Create your feedback tab with custom text and colors. Choose your language for the widget. Manage your categories and fields for feedback.</li>
              <li><b>Notifications</b>: Create short notification and add targeting rules to show it to a specific set of audience. Drive traffic. Increase conversion. Get real-time reports.</li>
              <li><b>Surveys</b>: Create surveys and add targeting rules for them. Modify CSS to customize the survey skin to match your site's look and feel. View realtime analytics & reports.</li>
            </ul>
          </li>
          <li>The widget is not appearing on your site even after saving the license code? This is most likely beacause you haven't verified your email yet. <a class="resend-email-link" href="#">Resend the activation email</a>.</li>
          <li>Can't access your dashboard? Try <a href="http://webengage.com/user/account.html?action=viewForgot">forgot password</a>. If nothing works, please <a href="http://webengage.com/contact">get in touch</a> with us.</li>
          <li>Choose from a range of <a href="http://webengage.com/pricing">pricing plans</a> that fits your requirement. Uprades or downgrades done in the dashboard reflect automatically on your site without changing anything in the plugin.</li>
        </ol>
      </div>
      <script>
        window.onload = function() {
          var resendLinks = document.getElementsByTagName('a');
          for(var i = 0; i < resendLinks.length; i++) {
                var resendLink = resendLinks[i];
                if(resendLink.className === 'resend-email-link') {
              resendLink.onclick = function() {
                var newFrame = document.createElement("iframe");
                newFrame.style.height = "0px";
                newFrame.setAttribute("marginheight", "0");
                newFrame.setAttribute("frameborder", "0");
                newFrame.setAttribute("src", "{$resend_email_url|escape:'htmlall':'UTF-8'}");
                document.body.appendChild(newFrame);
                return false;
              }
                }
          }
            }
      </script>
      <br class="clear"/>
    {/if}
  </div>
  <script type="text/javascript">
    if (document.getElementById('we-signup-iframe')) {
      var resizeIframe = function (height) {
      document.getElementById('we-signup-iframe').style.height = (parseInt(height) + 60) + "px";
    };
    
    if (typeof window['addEventListener'] !== 'undefined' && typeof window['postMessage'] !== 'undefined') {
      window.addEventListener("message", function (e) {
        if (e.origin.search("{$webengage_host_name|escape:'htmlall':'UTF-8'}") < 0) {
          return;
        }
        resizeIframe(e.data);
      }, false);
    }
    
    document.getElementById('we-signup-iframe').onload = function () {
      if (typeof window['addEventListener'] === 'undefined' || typeof window['postMessage'] === 'undefined') {
        document.getElementById('we-signup-iframe').style.height = "450px";
      }
      setTimeout(function () {
        if (document.getElementById('webengage-loading-info')) {
    document.getElementById('webengage-loading-info').style.display = 'none';
        }
      }, 500);
    };
  }
  </script>
</div>
