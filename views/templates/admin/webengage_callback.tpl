<?php
/**
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA &lt;contact@prestashop.com&gt;
*  @copyright 2007-2014 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
?>
{if isset($wlc)}
	<html>
		<body>
			<form id="postbackToWp" name="postbackToWp" method="post" action="{$main_url|escape:'htmlall':'UTF-8'}&option={$option|escape:'htmlall':'UTF-8'}" target="_top">
				<input type="hidden" name="weAction" value="wp-save"/>
				<input type="hidden" name="verification_message" value="{$vm|escape:'htmlall':'UTF-8'}"/>
				<input type="hidden" name="webengage_license_code" value="{$wlc|escape:'htmlall':'UTF-8'}"/>
				<input type="hidden" name="webengage_widget_status" value="{$wwa|escape:'htmlall':'UTF-8'}"/>
			</form>
			<script type="text/javascript">
				//document.getElementById("postbackToWp").setAttribute("action", parent.parent.window.location.href + "&noheader=true");
				document.getElementById("postbackToWp").submit();
			</script>
		</body>
	</html>
{/if}
