<?php
/**
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2013 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

	if (!defined('_PS_VERSION_')) exit;

	class WebEngage extends Module {
		private $html = '';
		protected $js_state = 0;
		protected $eligible = 0;
		protected static $products = array();

		public function __construct()
		{
			$this->name = 'webengage';
			$this->tab = 'advertising_marketing';
			$this->version = '1.0.1';
			$this->author = 'WebEngage';
			$this->module_key = 'c1f065917dd37420856006b40c497320';
			$this->need_instance = 0;
			$this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.6');

			parent::__construct();

			$this->displayName = $this->l('WebEngage Feedback, Survey and Notification');
			$this->description = $this->l('Get feedback and resolve support queries.
				Conduct short surveys. Display push notification messages.
				Love your customers? Get WebEngage now!.');
			$this->confirmUninstall = $this->l('Are you sure you want to uninstall WebEngage Module?');
			$this->checkContent();
			$this->context->smarty->assign('module_name', $this->name);
		}

		private function checkContent()
		{
			if (!Configuration::get('we_license_code') && !Configuration::get('we_status'))
				$this->warning = $this->l('You need to configure WebEngage. Please click on the configure link below module description');
		}

		public function install()
		{
			if (!parent::install() ||
				!$this->registerHook('header') ||
				!$this->registerHook('footer') ||
				!$this->registerHook('home') ||
				!$this->registerHook('productfooter') ||
				!$this->registerHook('displayBackOfficeHeader') ||
				!$this->initWebEngagePersistentData())
				return false;

			if (version_compare(_PS_VERSION_, '1.5', '>=') && !$this->registerHook('actionCartSave')) {
				return false;
			}

			return true;
		}

		public function uninstall()
		{
			if (!parent::uninstall() || !$this->deInitWebEngagePersistentData()) return false;

			return true;
		}

		public function initWebEngagePersistentData()
		{
			if (!Configuration::updateValue('we_license_code', '') || !Configuration::updateValue('we_status', ''))
				return false;

			return true;
		}

		public function deInitWebEngagePersistentData()
		{
			Configuration::deleteByName('we_license_code');
			Configuration::deleteByName('we_status');

			return true;
		}

		public function getContent()
		{
			if (!Tools::getValue('page'))
				return $this->handleMainPage();
			else {
				switch (Tools::getValue('page'))
				{
					case 'main':
						return $this->handleMainPage();

					case 'callback':
						return $this->handleCallbackPage();

					case 'resize':
						return $this->handleResizePage();
				}
			}
		}

		/**
		 * hook save cart event to implement addtocart and remove from cart functionality
		 */
		public function hookActionCartSave()
		{
			if (!isset($this->context->cart))
				return;

			if (!Tools::getIsset('id_product'))
				return;

			$cart = array(
				'controller' => Tools::getValue('controller'),
				'addAction' => Tools::getValue('add') ? 'add' : '',
				'removeAction' => Tools::getValue('delete') ? 'delete' : '',
				'extraAction' => Tools::getValue('op'),
				'qty' => (int)Tools::getValue('qty', 1)
			);

			$cart_products = $this->context->cart->getProducts();
			if (isset($cart_products) && count($cart_products))
				foreach ($cart_products as $cart_product)
					if ($cart_product['id_product'] == Tools::getValue('id_product'))
						$add_product = $cart_product;

			if ($cart['removeAction'] == 'delete')
			{
				$add_product_object = new Product((int)Tools::getValue('id_product'), true, (int)Configuration::get('PS_LANG_DEFAULT'));
				if (Validate::isLoadedObject($add_product_object))
				{
					$add_product['name'] = $add_product_object->name;
					$add_product['manufacturer_name'] = $add_product_object->manufacturer_name;
					$add_product['category'] = $add_product_object->category;
					$add_product['reference'] = $add_product_object->reference;
					$add_product['link_rewrite'] = $add_product_object->link_rewrite;
					$add_product['link'] = $add_product_object->link_rewrite;
					$add_product['price'] = $add_product_object->price;
					$add_product['ean13'] = $add_product_object->ean13;
					$add_product['id_product'] = Tools::getValue('id_product');
					$add_product['id_category_default'] = $add_product_object->id_category_default;
					$add_product['out_of_stock'] = $add_product_object->out_of_stock;
					$add_product = Product::getProductProperties((int)Configuration::get('PS_LANG_DEFAULT'), $add_product);
				}
			}

			if (isset($add_product) && !in_array((int)Tools::getValue('id_product'), self::$products))
			{
				self::$products[] = (int)Tools::getValue('id_product');
				$we_products = $this->wrapProduct($add_product, $cart, 0, true);

				if (array_key_exists('id_product_attribute', $we_products) && $we_products['id_product_attribute'] != '' && $we_products['id_product_attribute'] != 0)
					$id_product = $we_products['id_product_attribute'];
				else
					$id_product = Tools::getValue('id_product');

				if (isset($this->context->cookie->we_cart))
					$wecart = unserialize($this->context->cookie->we_cart);
				else
					$wecart = array();

				if ($cart['removeAction'] == 'delete')
					$we_products['quantity'] = -1;
				elseif ($cart['extraAction'] == 'down')
				{
					if (array_key_exists($id_product, $wecart))
						$we_products['quantity'] = $wecart[$id_product]['quantity'] - $cart['qty'];
					else
						$we_products['quantity'] = $cart['qty'] * -1;
				}
				elseif (Tools::getValue('step') <= 0) // Sometimes cartsave is called in checkout
				{
					if (array_key_exists($id_product, $wecart))
						$we_products['quantity'] = $wecart[$id_product]['quantity'] + $cart['qty'];
				}

				$wecart[$id_product] = $we_products;
				$this->context->cookie->we_cart = serialize($wecart);
			}
		}

		public function hookHeader()
		{
			if ($this->active && Configuration::get('we_license_code')) {
				$this->context->controller->addJs($this->_path.'js/WeAnalyticActionLib.js');
				return $this->_getWebEngageTag();
			}
		}

		/**
		 * hook product page footer to load JS for product details view
		 */
		public function hookProductFooter($params)
		{
			$controller_name = Tools::getValue('controller');
			if ($controller_name == 'product')
			{
				// Add product view
				$we_product = $this->wrapProduct((array)$params['product'], null, 0, true);
				$js = 'WEA.addProductDetailView('.Tools::jsonEncode($we_product).');';

				if (isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], $_SERVER['HTTP_HOST']) > 0)
					$js .= $this->addProductViewByHttpReferal(array($we_product));

				$this->js_state = 1;
				return $this->_runJs($js);
			}
		}

		/**
		 * Generate WebEngage Analytics js
		 */
		protected function _runJs($js_code, $backoffice = 0)
		{
			if (Configuration::get('we_license_code'))
			{
				$runjs_code = '';
				if (!empty($js_code))
					$runjs_code .= '
					<script type="text/javascript">
						var WEA = WeAnalyticEnhancedECommerce;'.$js_code.'
					</script>';

				if (($this->js_state) != 1 && ($backoffice == 0))
					$runjs_code .= '
					<script type="text/javascript">
						webengage.track(\'pageview\');
					</script>';

				return $runjs_code;
			}
		}

		public function hookBackOfficeHeader()
		{
			if (!(Tools::getValue('controller') == 'AdminModules' && Tools::getValue('configure') == 'webengage'))
				return;

			$js = '';
			if (strcmp(Tools::getValue('configure'), $this->name) === 0)
			{
				if (version_compare(_PS_VERSION_, '1.5', '>') == true)
				{
					$this->context->controller->addCSS($this->_path.'css/main.css');
				}
				else
				{
					$js .= '<link rel="stylesheet" href="'.$this->_path.'css/main.css" type="text/css" />';
				}
			}

			return $js;
		}

		protected function filter($we_scripts)
		{
			if ($this->filterable = 1)
				return implode(';', array_unique(explode(';', $we_scripts)));

			return $we_scripts;
		}

		/**
		 * hook home to display generate the product list associated to home featured, news products and best sellers Modules
		 */
		public function hookHome()
		{
			$we_scripts = '';

			// Home featured products
			if ($this->isModuleEnabled('homefeatured'))
			{
				$category = new Category($this->context->shop->getCategory(), $this->context->language->id);
				$home_featured_products = $this->wrapProducts($category->getProducts((int)Context::getContext()->language->id, 1,
				(Configuration::get('HOME_FEATURED_NBR') ? (int)Configuration::get('HOME_FEATURED_NBR') : 8), 'position'), array(), true);
				$we_scripts .= $this->addProductView($home_featured_products);
			}

			// New products
			if ($this->isModuleEnabled('blocknewproducts') && (Configuration::get('PS_NB_DAYS_NEW_PRODUCT')
					|| Configuration::get('PS_BLOCK_NEWPRODUCTS_DISPLAY')))
			{
				$new_products = Product::getNewProducts((int)$this->context->language->id, 0, (int)Configuration::get('NEW_PRODUCTS_NBR'));
				$new_products_list = $this->wrapProducts($new_products, array(), true);
				$we_scripts .= $this->addProductView($new_products_list);
			}

			// Best Sellers
			if ($this->isModuleEnabled('blockbestsellers') && (!Configuration::get('PS_CATALOG_MODE')
					|| Configuration::get('PS_BLOCK_BESTSELLERS_DISPLAY')))
			{
				$ga_homebestsell_product_list = $this->wrapProducts(ProductSale::getBestSalesLight((int)$this->context->language->id, 0, 8), array(), true);
				$we_scripts .= $this->addProductView($ga_homebestsell_product_list);
			}

			$this->js_state = 1;
			return $this->_runJs($this->filter($we_scripts));
		}

		/**
		 * hook footer to load JS script for standards actions such as product clicks
		 */
		public function hookFooter()
		{
			$we_scripts = '';
			$this->js_state = 0;

			if($this->context->cookie->logged) {
				$customerId = $this->context->cookie->id_customer;
				$we_scripts .= 'WEA.loginUser("'.$customerId.'");';

				$customer = new Customer((int)$customerId);

				// firstname and lastname
				$we_scripts .= 'WEA.setFullName("'.$this->context->cookie->customer_firstname.'", "'.$this->context->cookie->customer_lastname.'");';

				// company
				if(isset($customer->company) && !is_null($customer->company)) {
					$we_scripts .= 'WEA.setUserAttribute("we_companys", "'.$customer->company.'");';
				}

				// birthday
				if(isset($customer->birthday) && !is_null($customer->birthday)) {
					$we_scripts .= 'WEA.setUserAttribute("we_birth_date", "'.$customer->birthday.'");';
				}

				// gender
				if(isset($customer->id_gender) && !is_null($customer->id_gender)) {
					$gender = new Gender((int)$customer->id_gender);
					$genderString = ($gender->type === "0") ? "male" : "female";
					$we_scripts .= 'WEA.setUserAttribute("we_gender", "'.$genderString.'");';
				}
			} else {
				// guest id
				$we_scripts .= 'WEA.setUserAttribute("id_guest", "'.$this->context->cookie->id_guest.'");';
			}

			if (isset($this->context->cookie->we_cart))
			{
				$this->filterable = 0;

				$wecarts = unserialize($this->context->cookie->we_cart);
				foreach ($wecarts as $wecart)
				{
					if ($wecart['quantity'] > 0)
						$we_scripts .= 'WEA.addToCart('.Tools::jsonEncode($wecart).');';
					elseif ($wecart['quantity'] < 0)
					{
						$wecart['quantity'] = abs($wecart['quantity']);
						$we_scripts .= 'WEA.removeFromCart('.Tools::jsonEncode($wecart).');';
					}
				}
				unset($this->context->cookie->we_cart);
			}

			$controller_name = Tools::getValue('controller');
			$products = $this->wrapProducts($this->context->smarty->getTemplateVars('products'), array(), true);

			if ($controller_name == 'order' || $controller_name == 'orderopc')
			{
				$this->eligible = 1;
				$step = Tools::getValue('step');
				if (empty($step))
					$step = 0;
				$we_scripts .= $this->addProductFromCheckout($products, $step);
				$we_scripts .= 'WEA.addCheckout(\''.(int)$step.'\');';
			}

			if (version_compare(_PS_VERSION_, '1.5', '<'))
			{
				if ($controller_name == 'orderconfirmation')
					$this->eligible = 1;
			}
			else
			{
				$confirmation_hook_id = (int)Hook::getIdByName('orderConfirmation');
				if (isset(Hook::$executed_hooks[$confirmation_hook_id]))
					$this->eligible = 1;
			}

			if (isset($products) && count($products) && $controller_name != 'index')
			{
				if ($this->eligible == 0)
					$we_scripts .= $this->addProductView($products);
					//--$we_scripts .= $this->addProductClick($products);
			}

			return $this->_runJs($we_scripts);
		}

		protected function _getWebEngageTag(){
			return '<script id="_webengage_script_tag" type="text/javascript">var webengage; !function(e,t,n){function o(e,t){e[t[t.length-1]]=function(){r.__queue.push([t.join("."),arguments])}}var i,s,r=e[n],g=" ",l="init options track screen onReady".split(g),a="feedback survey notification".split(g),c="options render clear abort".split(g),p="Open Close Submit Complete View Click".split(g),u="identify login logout setAttribute".split(g);if(!r||!r.__v){for(e[n]=r={__queue:[],__v:"5.0",user:{}},i=0;i<l.length;i++)o(r,[l[i]]);for(i=0;i<a.length;i++){for(r[a[i]]={},s=0;s<c.length;s++)o(r[a[i]],[a[i],c[s]]);for(s=0;s<p.length;s++)o(r[a[i]],[a[i],"on"+p[s]])}for(i=0;i<u.length;i++)o(r.user,["user",u[i]]);setTimeout(function(){var f=t.createElement("script"),d=t.getElementById("_webengage_script_tag");f.type="text/javascript",f.async=!0,f.src=("https:"==t.location.protocol?"https://ssl.widgets.webengage.com":"http://cdn.widgets.webengage.com")+"/js/widget/webengage-min-v-5.0.js",d.parentNode.insertBefore(f,d)})}}(window,document,"webengage");webengage.init("' . Configuration::get('we_license_code') . '");</script>';
		}

		private function getWebEngageMainBaseUrl()
		{
			return $this->removeLastPart($this->fullUrl()).'/'
				.AdminController::$currentIndex.'&configure='.$this->name
				.'&token='.Tools::getAdminTokenLite('AdminModules');
		}

		private function removeLastPart($url)
		{
			$url = (Tools::substr($url, -1) == '/') ? Tools::substr($url, 0, -1) : $url;
			$urlparts = explode('/', $url);
			array_pop($urlparts);
			$url = implode($urlparts, '/');

			return $url;
		}

		private function fullUrl()
		{
			$s = empty($_SERVER['HTTPS']) ? '' : ($_SERVER['HTTPS'] == 'on') ? 's' : '';
			$protocol = Tools::substr(Tools::strtolower($_SERVER['SERVER_PROTOCOL']), 0,
				strpos(Tools::strtolower($_SERVER['SERVER_PROTOCOL']), '/')).$s;
			$port = ($_SERVER['SERVER_PORT'] == '80') ? '' : (':'.$_SERVER['SERVER_PORT']);
			$uri = $protocol.'://'.$_SERVER['SERVER_NAME'].$port.$_SERVER['REQUEST_URI'];
			$segments = explode('?', $uri, 2);
			$url = $segments[0];

			return $url;
		}

		private function handleMainPage()
		{
			$this->webengageProcessWebengageOptions();

			$webengage_host_name = 'webengage.com';
			$m_license_code = Configuration::get('we_license_code');
			$m_status = Configuration::get('we_status');

			$we_url_base = $this->getWebEngageMainBaseUrl();
			$main_url = $we_url_base.'&page=main';
			$next_url = $we_url_base.'&page=callback&noheader=true';
			$activation_url = $main_url.'&weAction=activate';

			$this->context->smarty->assign('m_license_code_old', $m_license_code);
			$this->context->smarty->assign('m_widget_status', $m_status);

			$this->context->smarty->assign('message', urldecode(Tools::getValue('message') ?
				htmlspecialchars(Tools::getValue('message'), ENT_COMPAT, 'UTF-8') : ''));
			$this->context->smarty->assign('error_message', urldecode(Tools::getValue('error-message') ?
				htmlspecialchars(Tools::getValue('error-message'), ENT_COMPAT, 'UTF-8') : ''));

			$this->context->smarty->assign('email', '');
			$this->context->smarty->assign('user_full_name', '');

			$this->context->smarty->assign('main_url', $main_url);
			$this->context->smarty->assign('next_url', $next_url);
			$this->context->smarty->assign('activation_url', $activation_url);

			if (isset($_SERVER['HTTP_HOST']))
				$this->context->smarty->assign('domain_name', $_SERVER['HTTP_HOST']);
			else
				$this->context->smarty->assign('domain_name', $_SERVER['SERVER_NAME']);

			$this->context->smarty->assign('domain_name', '');

			$this->context->smarty->assign('webengage_host_name', $webengage_host_name);
			$this->context->smarty->assign('webengage_host', 'http://'.$webengage_host_name);
			$this->context->smarty->assign('secure_webengage_host', 'https://'.$webengage_host_name);
			$this->context->smarty->assign('resend_email_url', '//'.$webengage_host_name.
				'/thirdparty/signup.html?action=resendVerificationEmail&licenseCode='
				.urlencode($m_license_code).'&next='.urlencode($next_url).
				'&activation_url='.urlencode($activation_url).'&channel=prestashop');

			return $this->display(__FILE__, 'views/templates/admin/webengage_main.tpl');
		}

		private function handleCallbackPage()
		{
			$we_url_base = $this->getWebEngageMainBaseUrl();
			$main_url = $we_url_base.'&page=main';

			$this->context->smarty->assign('main_url', $main_url);
			$this->context->smarty->assign('wlc', urldecode(Tools::getValue('webengage_license_code') ?
				htmlspecialchars(Tools::getValue('webengage_license_code'), ENT_COMPAT, 'UTF-8') : ''));
			$this->context->smarty->assign('vm', urldecode(Tools::getValue('verification_message') ?
				htmlspecialchars(Tools::getValue('verification_message'), ENT_COMPAT, 'UTF-8') : ''));
			$this->context->smarty->assign('wwa', urldecode(Tools::getValue('webengage_widget_status') ?
				htmlspecialchars(Tools::getValue('webengage_widget_status'), ENT_COMPAT, 'UTF-8') : ''));
			$this->context->smarty->assign('option', urldecode(Tools::getValue('option') ? Tools::getValue('option') : null));

		return $this->display(__FILE__, 'views/templates/admin/webengage_callback.tpl');
		}

		private function handleResizePage()
		{
			$this->context->smarty->assign('height', urldecode(Tools::getValue('height') ?
				htmlspecialchars(Tools::getValue('height'), ENT_COMPAT, 'UTF-8') : ''));

			return $this->display(__FILE__, 'views/templates/admin/webengage_resize.tpl');
		}

		private function webengageProcessWebengageOptions()
		{
			$main_url = AdminController::$currentIndex.'&configure='.$this->name.'&token='
				.Tools::getAdminTokenLite('AdminModules');

			if (Tools::getValue('weAction'))
			{
				if (Tools::getValue('weAction') === 'wp-save')
				{
					$message = $this->webengageUpdateWebengageOptions();
					$redirect_url = $main_url.'&'.$message[0].'='.urlencode($message[1]);
				}
				elseif (Tools::getValue('weAction') === 'reset')
				{
					$message = $this->webengageResetWebengageOptions();
					$redirect_url = $main_url.'&'.$message[0].'='.urlencode($message[1]);
				}
				elseif (Tools::getValue('weAction') === 'activate')
				{
					$message = $this->webengageActivateWeWidget();
					$redirect_url = $main_url.'&'.$message[0].'='.urlencode($message[1]);
				}
				elseif (Tools::getValue('weAction') === 'discardMessage')
				{
					$this->webengageDiscardStatusMessage();
					$redirect_url = $main_url;
				}

				if (Tools::strlen($redirect_url) > 0)
					Tools::redirectAdmin($redirect_url);
			}
		}

		/**
		* Discard message processor.
		*/
		private function webengageDiscardStatusMessage()
		{
			Configuration::updateValue('we_status', '');
		}

		/**
		* Resetting processor.
		*/
		private function webengageResetWebengageOptions()
		{
			Configuration::updateValue('we_license_code', '');
			Configuration::updateValue('we_status', '');
			return array('message', 'Your WebEngage options are deleted. You can signup for a new account.');
		}

		/**
		* Update processor.
		*/
		private function webengageUpdateWebengageOptions()
		{
			$wlc = Tools::getValue('webengage_license_code') ?
				htmlspecialchars(Tools::getValue('webengage_license_code'), ENT_COMPAT, 'UTF-8') : '';
			$vm = Tools::getValue('verification_message') ?
				htmlspecialchars(Tools::getValue('verification_message'), ENT_COMPAT, 'UTF-8') : '';
			$wws = Tools::getValue('webengage_widget_status') ?
				htmlspecialchars(Tools::getValue('webengage_widget_status'), ENT_COMPAT, 'UTF-8') : 'ACTIVE';
			if (!empty($wlc))
			{
				Configuration::updateValue('we_license_code', trim($wlc));
				Configuration::updateValue('we_status', $wws);
				$msg = !empty($vm) ? $vm : 'Your WebEngage widget license code has been updated.';
				return array('message', $msg);
			}
			else return array('error-message', 'Please add a license code.');
		}

		/**
		* Activate processor.
		*/
		private function webengageActivateWeWidget()
		{
			$wlc = Tools::getValue('webengage_license_code') ?
				htmlspecialchars(Tools::getValue('webengage_license_code'), ENT_COMPAT, 'UTF-8') : '';
			$old_value = Configuration::get('we_license_code');
			$wws = Tools::getValue('webengage_widget_status') ?
				htmlspecialchars(Tools::getValue('webengage_widget_status'), ENT_COMPAT, 'UTF-8') : 'ACTIVE';
			if ($wlc === $old_value)
			{
				Configuration::updateValue('we_status', $wws);
				$msg = 'Your plugin installation is complete. You can do further customizations from your WebEngage dashboard.';
				return array('message', $msg);
			}
			else
			{
				$msg = 'Unauthorized plugin activation request';
				return array('error-message', $msg);
			}
		}

		/**
		 * wrap product to provide a standard product information for WebEngage analytics script
		 */
		private function wrapProduct($product, $extras, $index = 0, $full = false)
		{
			$we_product = '';

			$variant = null;
			if (isset($product['attributes_small']))
				$variant = $product['attributes_small'];
			elseif (isset($extras['attributes_small']))
				$variant = $extras['attributes_small'];

			$product_qty = 1;
			if (isset($extras['qty']))
				$product_qty = $extras['qty'];
			elseif (isset($product['cart_quantity']))
				$product_qty = $product['cart_quantity'];

			$product_id = 0;
			if (!empty($product['id_product']))
				$product_id = $product['id_product'];
			else if (!empty($product['id']))
				$product_id = $product['id'];

			if (!empty($product['id_product_attribute']))
				$product_id .= '-'. $product['id_product_attribute'];

			$product_type = 'typical';
			if (isset($product['pack']) && $product['pack'] == 1)
				$product_type = 'pack';
			elseif (isset($product['virtual']) && $product['virtual'] == 1)
				$product_type = 'virtual';

			if ($full)
			{
				$we_product = array(
					'id' => $product_id,
					'name' => Tools::jsonEncode($product['name']),
					'category' => Tools::jsonEncode($product['category']),
					'brand' => isset($product['manufacturer_name']) ? Tools::jsonEncode($product['manufacturer_name']) : '',
					'variant' => Tools::jsonEncode($variant),
					'type' => $product_type,
					'position' => $index ? $index : '0',
					'quantity' => $product_qty,
					'list' => Tools::getValue('controller'),
					'url' => isset($product['link']) ? urlencode($product['link']) : '',
					'price' => number_format($product['price'], '2')
				);
			}
			else
			{
				$we_product = array(
					'id' => $product_id,
					'name' => Tools::jsonEncode($product['name'])
				);
			}
			return $we_product;
		}

		/**
		 * hook home to display generate the product list associated to home featured, news products and best sellers Modules
		 */
		public function isModuleEnabled($name)
		{
			if (version_compare(_PS_VERSION_, '1.5', '>='))
				if(Module::isEnabled($name))
				{
					$module = Module::getInstanceByName($name);
					return $module->isRegisteredInHook('home');
				}
				else
					return false;
			else
			{
				$module = Module::getInstanceByName($name);
				return ($module && $module->active === true);
			}
		}

		/**
		 * wrap products to provide a standard products information for WebEngage analytics script
		 */
		private function wrapProducts($products, $extras = array(), $full = false)
		{
			$result_products = array();
			if (!is_array($products))
				return;

			$currency = new Currency($this->context->currency->id);
			$usetax = (Product::getTaxCalculationMethod((int)$this->context->customer->id) != PS_TAX_EXC);

			if (count($products) > 20)
				$full = false;
			else
				$full = true;

			foreach ($products as $index => $product)
			{
				if ($product instanceof Product)
					$product = (array)$product;

				if (!isset($product['price']))
					$product['price'] = (float)Tools::displayPrice(Product::getPriceStatic((int)$product['id_product'], $usetax), $currency);
				$result_products[] = $this->wrapProduct($product, $extras, $index, $full);
			}

			return $result_products;
		}

		public function array_flat($array, $prefix = '') {
		    $result = array();

		    foreach ($array as $key => $value)
		    {
		        $new_key = $prefix . (empty($prefix) ? '' : '.') . $key;

		        if (is_array($value))
		        {
		            $result = array_merge($result, array_flat($value, $new_key));
		        }
		        else
		        {
		            $result[$new_key] = $value;
		        }
		    }

		    return $result;
		}

		/**
		 * add product impression js and product click js
		 */
		public function addProductView($products)
		{
			if (!is_array($products))
				return;

			$js = '';
			foreach ($products as $product)
				$js .= 'WEA.add('.Tools::jsonEncode($product).",'',true);";

			return $js;
		}

		public function addProductClick($products)
		{
			if (!is_array($products))
				return;

			$js = '';
			foreach ($products as $product)
				$js .= 'WEA.addProductClick('.Tools::jsonEncode($product).');';

			return $js;
		}

		public function addProductViewByHttpReferal($products)
		{
			if (!is_array($products))
				return;

			$js = '';
			foreach ($products as $product)
				$js .= 'WEA.addProductViewByHttpReferal('.Tools::jsonEncode($product).');';

			return $js;
		}

		/**
		 * Add product checkout info
		 */
		public function addProductFromCheckout($products)
		{
			if (!is_array($products))
				return;

			$js = '';
			foreach ($products as $product)
				$js .= 'WEA.add('.Tools::jsonEncode($product).');';

			return $js;
		}
	}
?>
